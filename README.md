This project is intended to test publishing and installing Helm charts using the GitLab Package Registry. 

To get started:

1. Install minikube `brew install minikube`, so that I have access to a Kubernetes cluster.
1. Install Kubernetes `brew install kubectl`.
1. Install Helm: `brew install helm`.
1. Install the Helm push plug-in `helm plugin install https://github.com/chartmuseum/helm-push.git`.
1. Create a new project.
1. Create a personal access token.
1. Add my GitLab project as a Helm repository. `helm repo add --username trizzi --password <personal_access_token> project-1 https://gitlab.com/api/v4/projects/28024541/packages/helm/stable
1. Create a Helm chart `helm create trizzi-flow`.
1. Package the chart for distribution `helm package trizzi-flow`
1. Confirm that `trizzi-flow-0.1.0.tgz` was added to the repository
1. helm push `trizzi-flow-0.1.0.tgz project-1`
1. Check this project's [package registry](https://gitlab.com/gitlab-org/ci-cd/package-stage/feature-testing/helm-chart-example/-/packages) for the package
1. Run `helm repo update` prior to installing the package
1. Start my Kubernetes cluster `minikube start`
1. Install the chart `helm install my-release project-1/trizzi-flow`
1. Huzzah!

